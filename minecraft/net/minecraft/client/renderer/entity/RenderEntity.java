package net.minecraft.client.renderer.entity;

import net.minecraft.entity.Entity;
import net.minecraft.util.ResourceLocation;
import org.lwjgl.opengl.GL11;

public class RenderEntity extends Render {
   private static final String __OBFID = "CL_00000986";

   public void doRender(Entity par1Entity, double par2, double par4, double par6, float par8, float par9) {
      GL11.glPushMatrix();
      renderOffsetAABB(par1Entity.boundingBox, par2 - par1Entity.lastTickPosX, par4 - par1Entity.lastTickPosY, par6 - par1Entity.lastTickPosZ);
      GL11.glPopMatrix();
   }

   protected ResourceLocation getEntityTexture(Entity par1Entity) {
      return null;
   }
}
