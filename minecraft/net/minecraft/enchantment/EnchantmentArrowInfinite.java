package net.minecraft.enchantment;

public class EnchantmentArrowInfinite extends Enchantment {
   private static final String __OBFID = "CL_00000100";

   public EnchantmentArrowInfinite(int par1, int par2) {
      super(par1, par2, EnumEnchantmentType.bow);
      this.setName("arrowInfinite");
   }

   public int getMinEnchantability(int par1) {
      return 20;
   }

   public int getMaxEnchantability(int par1) {
      return 50;
   }

   public int getMaxLevel() {
      return 1;
   }
}
