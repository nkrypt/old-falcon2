package falcon2.gui.override;

import falcon2.gui.GuiAccountLogin;
import net.minecraft.client.gui.*;

public class GuiMainMenuOverride extends GuiMainMenu {
	@Override
	public void initGui() {
		super.initGui();
		int var3 = height / 4 + 48;
		buttonList.add(new GuiButton(6, width / 2 - 100, var3 + (24 * 2), "Account Login"));
	}
	
	@Override
	public void actionPerformed(GuiButton guiButton) {
		super.actionPerformed(guiButton);
		switch(guiButton.id) { 
		case 6:
			mc.displayGuiScreen(new GuiAccountLogin(this));
			break;
		}
	}
	
	@Override
	public void drawScreen(int i, int j, float k) {
		super.drawScreen(i, j, k);
		drawString(fontRendererObj, "Falcon\247c2", 2, height - 20, -1);
	}
}
