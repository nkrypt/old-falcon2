package falcon2.gui;

import net.minecraft.client.gui.Gui;
import falcon2.*;
import falcon2.modules.Module;

public class GuiInGame extends Wrapper {
	public void draw() {
		int width = getMinecraft().displayWidth / 2;
		int height = getMinecraft().displayHeight / 2;
		int y = 2;
		Falcon2.getFonts().font1.drawStringWithShadow("Falcon", 2, y, 0xffffffff);
		Falcon2.getFonts().font1b.drawStringWithShadow("\247c2", 32, y, 0xffffffff); y += 9;
		String fps = "FPS: \247e" + Wrapper.getMinecraft().debug.split(" ")[0];
		Falcon2.getFonts().font2.drawStringWithShadow(fps, 2, y, 0xffffffff);
		y = 0;
		int longest = getLongestArrayItem();
		for (Module module : Falcon2.getModuleManager().getModules()) {
			if (module.getEnabled() && module.getOnArray() && !module.getHidden()) {
				int strWidth = Falcon2.getFonts().font2.getStringWidth(module.getName());
				Falcon2.getFonts().font2.drawStringWithShadow(module.getName(), (width  - strWidth) - 3, y + 3, module.getArrayColor());
				y += 10;
			}
		}
	}
	
	private int getLongestArrayItem() {
		int length = 0;
		for (Module module : Falcon2.getModuleManager().getModules()) {
			if (module.getEnabled() && module.getOnArray() && !module.getHidden()) {
				int strWidth = Falcon2.getFonts().font2.getStringWidth(module.getName());
				if (strWidth > length)
					length = strWidth;
			}
		}
		return length;
	}
}
