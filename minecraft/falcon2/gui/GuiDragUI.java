package falcon2.gui;

import java.awt.Color;
import java.util.ArrayList;

import org.lwjgl.opengl.GL11;

import falcon2.Falcon2;
import falcon2.gui.dragui.Panel;
import falcon2.gui.dragui.item.*;
import falcon2.modules.Category;
import falcon2.modules.Module;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiScreen;
import static falcon2.Fonts.*;

public class GuiDragUI extends GuiScreen {
	private static ArrayList<Panel> panels = new ArrayList<Panel>();
	
	public GuiDragUI() {
		if (!panels.isEmpty())
			return;
		int x = 5;
		int y = 5;
		int width = 110;
		int height = 15;
		for (Category category : Category.values()) {
			if (getModuleCountInCategory(category) > 0) {
				panels.add(new Panel(Category.fixCapitalization(category.name()), category, x, y, width, height) {
					@Override
					public void setupItems() {
						if (getCategory() == Category.VALUE) { 
							addValueItems(getItems());
						} else {
							for (Module module : Falcon2.getModuleManager().getModules()) {
								if (module.getCategory() == getCategory() && !module.getHidden())
									getItems().add(new ItemButton(module, getCategory()));
							}
						}
					}
				});
				x += width + 5;
				if (x + width > (Minecraft.getMinecraft().displayWidth / 2) - 5) {
					x = 5;
					y += 5 + height;
				}
			}
		}
	}
	
	@Override
	public void drawScreen(int i, int j, float k) {
		drawDefaultBackground();
		for (Panel panel : panels)
			panel.drawScreen(i, j, k);
	}
	
	@Override
	public void mouseClicked(int i, int j, int k) {
		for (Panel panel : panels)
			panel.mouseClicked(i, j, k);
	}
	
	@Override
	public void mouseMovedOrUp(int i, int j, int k) {
		for (Panel panel : panels)
			panel.mouseMovedOrUp(i, j, k);
	}
	
	private int getModuleCountInCategory(Category category) {
		int count = 0;
		for (Module module : Falcon2.getModuleManager().getModules()) {
			if (module.getCategory() == category)
				count++;
		}
		return count + (category == Category.VALUE ? 1 : 0);
	}
	
	private void addValueItems(ArrayList<Item> items) {
		items.add(new ItemSlider("Test Slider", 5.0F, 25.0F, 13.0F));
	}
}
