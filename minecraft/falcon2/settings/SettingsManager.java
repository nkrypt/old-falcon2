package falcon2.settings;

import java.util.ArrayList;
import falcon2.modules.*;

public class SettingsManager {
	private static ArrayList<Setting> settings = new ArrayList<Setting>();
	
	public SettingsManager() {
		settings.add(new SettingSearchDistance());
		settings.add(new SettingVelStepShortArrow());
	}
	
	public ArrayList<Setting> getSettings() {
		return settings;
	}
	
	class SettingSearchDistance extends Setting {
		public SettingSearchDistance() {
			super("searchDistance", Setting.SettingType.INTEGER);
		}
		
		@Override
		public Object getSetting() {
			return Search.distance;
		}
		
		@Override
		public void setSetting(Object setting) {
			Search.distance = (Integer) setting;
		}
	}
	
	class SettingVelStepShortArrow extends Setting {
		public SettingVelStepShortArrow() {
			super("velStepShortArrow", Setting.SettingType.BOOLEAN);
		}
		
		@Override
		public Object getSetting() {
			return VelStep.shortArrow;
		}
		
		@Override
		public void setSetting(Object setting) {
			VelStep.shortArrow = (Boolean) setting;
		}
	}
}
