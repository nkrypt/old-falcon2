package falcon2.modules;

import java.util.ArrayList;

import net.minecraft.entity.Entity;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.StringUtils;
import falcon2.Falcon2;
import falcon2.modules.commands.Command;
import falcon2.modules.commands.CommandManager;

public class Friends extends Module {
	private static ArrayList<String> friends = new ArrayList<String>();
	
	public Friends() {
		super("Friends", null, null, false);
		setHidden(true);
	}
	
	@Override
	public void loadCommands(CommandManager commandManager) {
		commandManager.getCommands().add(new Command("f") {
			@Override
			public boolean runCommand(String arguments) {
				if (arguments.contains(" ")) {
					String args[] = arguments.split(" ");
					if (args[0].equalsIgnoreCase("add")) {
						String name = args[1];
						if (!checkFriend(name)) {
							friends.add(name.toLowerCase());
							addChatMessage("Added '\247e" + name + "\247f' to your friends list.");
							Falcon2.getFileManager().saveFile(Falcon2.getFileManager().getFileInstances().fileHandlerFriends);
						} else {
							addChatMessageError("'\247e" + name + "\247f' is already on your friends list.");
						}
						return true;
					} else if (args[0].equalsIgnoreCase("rem")) {
						String name = args[1];
						if (checkFriend(name)) {
							friends.remove(name.toLowerCase());
							addChatMessage("Removed '\247e" + name + "\247f' from your friends list.");
							Falcon2.getFileManager().saveFile(Falcon2.getFileManager().getFileInstances().fileHandlerFriends);
						} else {
							addChatMessageError("'\247e" + name + "\247f' is not on your friends list.");
						}
						return true;
					}
				}
				return false;
			}
		});
	}
	
	public static boolean checkFriend(String name) {
		return friends.contains(name.toLowerCase());
	}
	
	public static ArrayList<String> getFriends() {
		return friends;
	}
}
