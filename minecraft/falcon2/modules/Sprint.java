package falcon2.modules;

import net.minecraft.block.Block;
import net.minecraft.block.BlockLiquid;
import net.minecraft.block.material.Material;

import org.lwjgl.input.Keyboard;

import falcon2.Falcon2;
import falcon2.modules.commands.*;
import falcon2.modules.events.*;

public class Sprint extends Module {
	public static float speed = 1.0F;
	private static Long last = null;
	
	public Sprint() {
		super("Sprint", Keyboard.KEY_M, Category.MOVEMENT, false);
	}
	
	@Override
	public void onEvent(Event event) {
		if (!getEnabled())
			return;
		if (!(getPlayer().movementInput.moveForward > 0.25F))
			return;
		if (event instanceof EventOnUpdate) {
			EventOnUpdate event2 = (EventOnUpdate) event;
			if (event2.getState() == State.POST) {
				if (getModuleByName("Aura").getEnabled()) {
					getPlayer().setSprinting(Aura.target == null);
				} else {
					getPlayer().setSprinting(true);
				}
			}
		} else if (event instanceof EventPlayerSpeed) {
			if (!(getPlayer().movementInput.moveForward > 0.25F))
				return;
			EventPlayerSpeed event2 = (EventPlayerSpeed) event;
			if (!getModuleByName("Phase").getEnabled())
				event2.setSpeed(speed);
		}
	}
	
	@Override
	public void loadCommands(CommandManager commandManager) {
		commandManager.getCommands().add(new Command("ss") {
			@Override
			public boolean runCommand(String arguments) {
				speed = Float.parseFloat(arguments);
				addChatMessage("Sprint speed set to '\247e" + speed + "\247f'.");
				return true;
			}
		});
	}
}
