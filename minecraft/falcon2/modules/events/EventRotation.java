package falcon2.modules.events;

public class EventRotation extends Event {
	private float yaw;
	private float pitch;
	
	public EventRotation(float yaw, float pitch) {
		this.yaw = yaw;
		this.pitch = pitch;
	}
	
	public float getYaw() {
		return yaw;
	}
	
	public float getPitch() {
		return pitch;
	}
	
	public void setRotation(Float yaw, Float pitch) {
		if (yaw != null)
			this.yaw = yaw;
		if (pitch != null)
			this.pitch = pitch;
	}
}
