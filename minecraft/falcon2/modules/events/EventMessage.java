package falcon2.modules.events;

public class EventMessage extends Event {
	private String message;
	
	public EventMessage(String message) {
		this.message = message;
	}
	
	public String getMessage() {
		return message;
	}
	
	public void setMessage(String message) {
		this.message = message;
	}
}
