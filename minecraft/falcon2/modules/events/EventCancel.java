package falcon2.modules.events;

public class EventCancel extends Event {
	private boolean canceled;
	
	public boolean getCanceled() {
		return canceled;
	}
	
	public void setCanceled(boolean canceled) {
		this.canceled = canceled;
	}
}
