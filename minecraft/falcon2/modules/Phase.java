package falcon2.modules;

import org.lwjgl.input.Keyboard;
import falcon2.modules.events.*;

public class Phase extends Module {
	public Phase() {
		super("Phase", Keyboard.KEY_P, Category.MOVEMENT, false);
	}
	
	@Override
	public void onEvent(Event event) {
		if (!getEnabled())
			return;
		if (event instanceof EventOnUpdate) {
			EventOnUpdate event2 = (EventOnUpdate) event;
			if (event2.getState() == State.POST) {
				getPlayer().noClip = true;
				getPlayer().motionY = 0D;
				getPlayer().setSprinting(true);
			}
		} else if (event instanceof EventPlayerSpeed) {
			EventPlayerSpeed event2 = (EventPlayerSpeed) event;
			event2.setSpeed(0.5F);
		}
	}
	
	@Override
	public void onEnabled() {
		getPlayer().noClip = true;
		super.onEnabled();
	}
	
	@Override
	public void onDisabled() {
		super.onDisabled();
		getPlayer().noClip = false;
	}
}
