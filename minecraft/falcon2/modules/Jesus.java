package falcon2.modules;

import net.minecraft.block.Block;
import net.minecraft.block.BlockLiquid;
import net.minecraft.block.material.Material;
import net.minecraft.network.play.client.C03PacketPlayer;

import org.lwjgl.input.Keyboard;

import falcon2.Falcon2;
import falcon2.modules.commands.*;
import falcon2.modules.events.*;

public class Jesus extends Module {
	private static Long last = null;
	public static boolean run = false;
	
	public Jesus() {
		super("Jesus", Keyboard.KEY_J, Category.MOVEMENT, false);
	}
	
	@Override
	public void onEvent(Event event) {
		if (!getEnabled())
			return;
		if (!getPlayer().isInWater())
			return;
		if (event instanceof EventOnUpdate) {
			EventOnUpdate event2 = (EventOnUpdate) event;
			if (event2.getState() == State.POST) {
				if (!isOverWater())
					return;
				getPlayer().onGround = true;
				getPlayer().motionY = 0;
				if (last == null || getSysTime() >= last + 180L) {
					last = getSysTime();
					getSendQueue().addToSendQueue(new C03PacketPlayer.C04PacketPlayerPosition(
							getPlayer().posX,
							getPlayer().posY - 1.722,
							getPlayer().boundingBox.minY,
							getPlayer().posZ,
							true));
				}
			}
		}
	}
	
	private boolean isOverWater() {
		double minX = getPlayer().boundingBox.minX;
		double minY = getPlayer().boundingBox.minY;
		double minZ = getPlayer().boundingBox.minZ;
		double maxX = getPlayer().boundingBox.maxX;
		double maxY = getPlayer().boundingBox.maxY;
		double maxZ = getPlayer().boundingBox.maxZ;
		double posX = getPlayer().posX;
		double posY = getPlayer().posY;
		double posZ = getPlayer().posZ;
		Block block = getWorld().getBlock((int) posX, (int) posY, (int) posZ);
		return block != null
				
				&& block instanceof BlockLiquid;
	}
}
