package falcon2.modules;

import java.util.ArrayList;

import net.minecraft.block.Block;
import net.minecraft.entity.Entity;
import net.minecraft.network.play.client.C03PacketPlayer;

import org.lwjgl.input.Keyboard;

import falcon2.Falcon2;
import falcon2.Wrapper;
import falcon2.modules.commands.Command;
import falcon2.modules.events.Event;

public class ModuleManager extends Wrapper {
	private static ArrayList<Module> modules;
	
	public ModuleManager() {
		if (modules != null)
			return;
		modules = new ArrayList<Module>();
		modules.add(new AimBot());
		modules.add(new Aura());
		modules.add(new AutoRespawn());
		modules.add(new AutoSoup());
		modules.add(new AntiCensor());
		modules.add(new AntiVelocity());
		modules.add(new Criticals());
		modules.add(new Friends());
		modules.add(new Fullbright());
		modules.add(new GodMode());
		modules.add(new Jesus());
		modules.add(new Parkour());
		modules.add(new Phase());
		modules.add(new PlayerESP());
		modules.add(new RemoteView());
		modules.add(new Retard());
		modules.add(new SafeWalk());
		modules.add(new Search());
		modules.add(new Sprint());
		modules.add(new Step());
		modules.add(new Tracers());
		modules.add(new TTF());
		modules.add(new VelStep());
		addCommands();
		Falcon2.setSettingsManager();
		Falcon2.setFileManager();
	}
	
	public static ArrayList<Module> getModules() {
		return modules;
	}
	
	public void callEvent(Event event) {
		for (Module module : getModules()) {
			try {
				module.onEvent(event);
			} catch (Exception e) {}
		}
	}
	
	public void addCommands() {
		for (Module module : getModules())
			module.loadCommands(Falcon2.getCommandManager());
		Falcon2.getCommandManager().getCommands().add(new Command("bsearch") {
			@Override
			public boolean runCommand(String arguments) {
				ArrayList<Block> blocks = new ArrayList<Block>();
				for (int id = 0; id < 380; id++) {
					Block block = Block.getBlockById(id);
					if (block != null)
						blocks.add(block);
				}
				for (Block block : blocks) {
					if (block.getLocalizedName().toLowerCase().contains(arguments.toLowerCase())) {
						int id = Block.getIdFromBlock(block);
						addChatMessage("Block Search: \247eName: " + block.getLocalizedName() + "\247f, \247eID: " + id);
					}
				}
				return true;
			}
		});
		Falcon2.getCommandManager().getCommands().add(new Command("die") {
			@Override
			public boolean runCommand(String arguments) {
				getSendQueue().addToSendQueue(new C03PacketPlayer.C04PacketPlayerPosition(getPlayer().posX, getPlayer().boundingBox.minY + 0.125D, getPlayer().posY + 0.125D, getPlayer().posZ, false));
				getPlayer().noClip = true;
				getPlayer().motionY = -999D;
				return true;
			}
		});
		Falcon2.getCommandManager().getCommands().add(new Command("help") {
			@Override
			public boolean runCommand(String arguments) {
				addChatMessage("Command: \247e.as delay [value] - Sets the AutoSoup eat delay.");
				addChatMessage("Command: \247e.as id [id] - Sets the AutoSoup item.");
				addChatMessage("Command: \247e.as threshold [value] - Sets the AutoSoup threshold.");
				addChatMessage("Command: \247e.ar aps [value] - Sets the Aura aps.");
				addChatMessage("Command: \247e.ar reach [value] - Sets the Aura reach.");
				addChatMessage("Command: \247e.bsearch [name] - Shows you the IDs blocks/items containing the name you have entered.");
				addChatMessage("Command: \247e.die - Kills you.");
				addChatMessage("Command: \247e.f add [name] - Adds a friend.");
				addChatMessage("Command: \247e.f rem [name] - Removes a friend.");
				addChatMessage("Command: \247e.help - Displays commands.");
				addChatMessage("Command: \247e.pkd [value] - Sets the Parkour fall distance.");
				addChatMessage("Command: \247e.mods - Displays all modules.");
				addChatMessage("Command: \247e.rv [name] - Sets the player for RemoteView to show.");
				addChatMessage("Command: \247e.rv clear - Deactivates RemoteView and shows your player's view.");
				addChatMessage("Command: \247e.sh add [id] [color],[color],[color] - Adds a new block to Search by it's ID and color.");
				addChatMessage("Command: \247e.sh distance [value] - Sets the distance in blocks that Search will render.");
				addChatMessage("Command: \247e.sh rem [id] - Removes block from Search by it's ID.");
				addChatMessage("Command: \247e.ss [value] - Sets the Sprint speed.");
				addChatMessage("Command: \247e.t [name] - Toggles specified module.");
				addChatMessage("Command: \247e.vsa - Toggles VelStep short arrow mode.");
				return true;
			}
		});
		Falcon2.getCommandManager().getCommands().add(new Command("mods") {
			@Override
			public boolean runCommand(String arguments) {
				for (Module module : getModules()) {
					if (module.getCategory() != null) {
						String s = "Module: \247e" + module.getName() + "\247f";
						if (module.getKeybind() != null)
							s += ", Keybind: \247e" + Keyboard.getKeyName(module.getKeybind()) + "\247f";
						s += ", Category: \247e" + Category.fixCapitalization(module.getCategory().name()) + "\247f";
						addChatMessage(s);
					}
				}
				return true;
			}
		});
		Falcon2.getCommandManager().getCommands().add(new Command("t") {
			@Override
			public boolean runCommand(String arguments) {
				if (arguments.length() > 0) {
					if (arguments.contains(", ")) {
						String args[] = arguments.split(", ");
						for (int i = 0; i < args.length; i++) {
							for (Module module : getModules()) {
								if (module.getName().toLowerCase().equals(args[i].toLowerCase()) && !module.getHidden()) {
									module.toggle();
									addChatMessage(module.getName() + " " + (module.getEnabled() ? "enabled":"disabled") + ".");
								}
							}
						}
						return true;
					} else {
						Module module = getModuleByName(arguments);
						if (module != null && !module.getHidden()) {
							module.toggle();
							addChatMessage(module.getName() + " " + (module.getEnabled() ? "enabled":"disabled") + ".");
							return true;
						} else {
							addChatMessageError("The module specified, is invalid.");
							return true;
						}
					}
				} else {
					addChatMessageError("Please specify a module name.");
					return true;
				}
			}
		});
	}
	
	public Module getModuleByName(String name) {
		Module module = null;
		for (Module module2 : getModules()) {
			if (module2.getName().toLowerCase().equals(name.toLowerCase())) {
				module = module2;
				break;
			}
		}
		return module;
	}
}