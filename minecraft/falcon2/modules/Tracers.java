package falcon2.modules;

import org.lwjgl.opengl.GL11;
import falcon2.Falcon2;
import falcon2.modules.events.*;
import net.minecraft.client.renderer.entity.RenderManager;
import net.minecraft.entity.*;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.StringUtils;

public class Tracers extends Module {
	public Tracers() {
		super("Tracers", null, Category.RENDER, false);
	}

	@Override
	public void onEvent(Event event) {
		if (!getEnabled())
			return;
		if (event instanceof EventRender) {
			for (Entity entity : getWorld().loadedEntityList) {
				if (entity != getPlayer() && entity instanceof EntityLivingBase)
					drawTracer(entity, Aura.target);
			}
		}
	}

	private void drawTracer(Entity entity, Entity target) {
		if (!(entity instanceof EntityPlayer))
			return;
		boolean isAuraTarget = getModuleByName("Aura").getEnabled() && entity == target;
		float xDiff = (float) (entity.posX - RenderManager.renderPosX);
		float yDiff = (float) (entity.boundingBox.minY - RenderManager.renderPosY);
		float zDiff = (float) (entity.posZ - RenderManager.renderPosZ);
		float distance = this.getPlayer().getDistanceToEntity(entity);
		boolean friend = false;
		EntityPlayer entityPlayer = (EntityPlayer) entity;
		String username = StringUtils.stripControlCodes(entityPlayer.getCommandSenderName());
		friend = Friends.checkFriend(username);
		renderUtils.start3DGLConstants();
		GL11.glEnable(GL11.GL_LINE_SMOOTH);
		GL11.glHint(GL11.GL_LINE_SMOOTH_HINT, GL11.GL_FASTEST);
		if (isAuraTarget) {
			GL11.glColor4f(0F, 1F, 0F, 0.8F);
		} else if (friend) {
			GL11.glColor4f(92F / 255F, 191F / 255F, 1F, 0.6F);
		} else if (distance >= 64f) {
			GL11.glColor4f(0F, 1F, 0F, 0.4F);
		} else {
			float green = (distance / 64F);
			GL11.glColor4f(1F, green, 0F, 0.4F);
		}	
		GL11.glLineWidth(isAuraTarget ? 2.5F : 1.5F);
		GL11.glBegin(GL11.GL_LINES);
		GL11.glVertex2f(0f, 0f);
		GL11.glVertex3f(xDiff, yDiff + 1F, zDiff);
		GL11.glEnd();
		GL11.glDisable(GL11.GL_LINE_SMOOTH);
		renderUtils.finish3DGLConstants();
	}
}
