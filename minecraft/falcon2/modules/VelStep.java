package falcon2.modules;

import net.minecraft.item.Item;

import org.lwjgl.input.Keyboard;

import falcon2.Falcon2;
import falcon2.modules.commands.Command;
import falcon2.modules.commands.CommandManager;
import falcon2.modules.events.*;

public class VelStep extends Module {
	private static int delay;
	private static boolean ready;
	private static int cooldown;
	private static int velocityCount;
	public static boolean shortArrow;
	private static int arrowDelay;
	
	public VelStep() {
		super("VelStep", Keyboard.KEY_L, Category.MOVEMENT, false);
	}
	
	@Override
	public void onEvent(Event event) {
		if (!getEnabled())
			return;
		if (getModuleByName("Phase").getEnabled())
			return;
		if (cooldown >= 50) {
			cooldown = -1;
		} else if (cooldown > -1) {
			cooldown++;
			return;
		}
		if (event instanceof EventVelocity) {
			EventVelocity event2 = (EventVelocity) event;
			if (event2.getEntity() != null) {
				if (event2.getEntity() == getPlayer() && getEnabled()) {
					event2.setCanceled(true);
					if (getPlayer().isCollidedHorizontally && getPlayer().movementInput.moveForward != 0.0F) {
						velocityCount++;
						if (velocityCount == 1) {
							new Thread(new Runnable() {
								@Override
								public void run() {
									try { Thread.sleep(2500L); } catch (Exception e) {}
									if (velocityCount < 2)
										velocityCount = 0;
								}
							}).start();
						}
						if (velocityCount >= 2) {
							velocityCount = 0;
							new Thread(new Runnable() {
								@Override
								public void run() {
									try { Thread.sleep(250L); } catch (Exception e) {}
									getPlayer().stepHeight = 8L;
									try { Thread.sleep(100L); } catch (Exception e) {}
									getPlayer().stepHeight = 0.5F;
								}
							}).start();
						}
					}
				}
			} else {
				event2.setCanceled(true);
			}
		} else if (event instanceof EventOnUpdate) {
			EventOnUpdate event2 = (EventOnUpdate) event;
			if (event2.getState() == State.POST) {
				boolean bowCheck = Item.getIdFromItem(getPlayer().inventory.getStackInSlot(getPlayer().inventory.currentItem).getItem()) == 261;
				if (bowCheck && getPlayer().isCollidedHorizontally && getPlayer().movementInput.moveForward != 0.0F && getGameSettings().keyBindUseItem.pressed && shortArrow) {
					arrowDelay++;
					if (arrowDelay >= 3) {
						arrowDelay = 0;
						getGameSettings().keyBindUseItem.pressed = false;
					}
				} else {
					if (arrowDelay > 0)
						arrowDelay = 0;
				}
			}
		}
	}
	
	@Override
	public void loadCommands(CommandManager commandManager) {
		commandManager.getCommands().add(new Command("vsa") {
			@Override
			public boolean runCommand(String arguments) {
				shortArrow = !shortArrow;
				addChatMessage("VelStep short arrow mode " + (shortArrow ? "enabled":"disabled") + ".");
				Falcon2.getFileManager().saveFile(Falcon2.getFileManager().getFileInstances().fileHandlerSettings);
				return true;
			}
		});
	}
	
	@Override
	public void onDisabled() {
		super.onDisabled();
		if (arrowDelay > 0)
			arrowDelay = 0;
	}
}
