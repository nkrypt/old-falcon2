package falcon2.modules;

import java.util.ArrayList;

import net.minecraft.entity.Entity;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.util.StringUtils;
import falcon2.modules.commands.Command;
import falcon2.modules.commands.CommandManager;
import falcon2.modules.events.*;

public class RemoteView extends Module {
	private Entity player;
	
	public RemoteView() {
		super("RemoteView", null, Category.RENDER, false);
		setHidden(true);
	}
	
	@Override
	public void onEvent(Event event) {
		if (event instanceof EventOnUpdate) {
			EventOnUpdate event2 = (EventOnUpdate) event;
			if (event2.getState() == State.POST) {
				if (player != null) {
					if (isEntityApproved()) {
						getMinecraft().renderViewEntity = (EntityLivingBase) player;
					} else {
						player = null;
					}
				} else {
					getMinecraft().renderViewEntity = null;
				}
			}
		}
	}
	
	@Override
	public void loadCommands(CommandManager commandManager) {
		commandManager.getCommands().add(new Command("rv") {
			@Override
			public boolean runCommand(String arguments) {
				if (arguments.equalsIgnoreCase("clear")) {
					player = null;
					addChatMessage("RemoteView player cleared.");
				} else {
					Entity player2 = null;
					for (Entity entity : new ArrayList<Entity>(getWorld().playerEntities)) {
						if (entity != getPlayer() && StringUtils.stripControlCodes(entity.getCommandSenderName()).equals(arguments)) {
							player2 = entity;
							break;
						}
					}
					if (player2 != null) {
						player = player2;
						addChatMessage("RemoteView player set to '\247e" + arguments + "\247f'.");
					} else {
						addChatMessageError("'\247e" + arguments + "\247f' could not be found.");
					}
				}
				return true;
			}
		});
	}
	
	private boolean isEntityApproved() {
		boolean foundEntity = false;
		for (Entity entity : new ArrayList<Entity>(getWorld().playerEntities)) {
			if (!foundEntity && entity == player && !entity.isDead)
				foundEntity = true;
		}
		if (!foundEntity)
			return false;
		return true;
	}
}
