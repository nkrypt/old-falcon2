package falcon2.modules;

import org.lwjgl.input.Keyboard;
import falcon2.modules.events.*;

public class Fullbright extends Module {
	public Fullbright() {
		super("Fullbright", Keyboard.KEY_B, Category.WORLD, false);
	}
	
	@Override
	public void onEvent(Event event) {
		if (!getEnabled())
			return;
		if (event instanceof EventBrightness) {
			EventBrightness event2 = (EventBrightness) event;
			event2.setBrightness(10.0F);
		}
	}
}
