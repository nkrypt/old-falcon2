package falcon2.file.files;

import java.io.File;
import java.util.ArrayList;
import falcon2.file.FileHandler;
import falcon2.file.FileUtils;
import falcon2.modules.Friends;

public class FileHandlerFriends extends FileHandler {
	public FileHandlerFriends() {
		super(new File("friends.txt"));
	}
	
	@Override
	public void saveFile() {
		ArrayList<String> contents = new ArrayList<String>();
		for (String friend : Friends.getFriends()) {
			if (!contents.contains(friend))
				contents.add(friend + "\r\n");
		}
		FileUtils.writeFile(getFile(), contents);
	}
	
	@Override
	public void loadFile() {
		if (!doesFileExist())
			return;
		ArrayList<String> contents = FileUtils.readFile(getFile());
		for (String line : contents) {
			line = line.toLowerCase().trim();
			if (line.length() > 0) {
				if (!Friends.getFriends().contains(line))
					Friends.getFriends().add(line);
			}
		}
	}
}
