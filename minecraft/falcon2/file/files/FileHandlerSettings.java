package falcon2.file.files;

import java.io.File;
import java.util.ArrayList;
import falcon2.Falcon2;
import falcon2.file.FileHandler;
import falcon2.file.FileUtils;
import falcon2.settings.Setting;

public class FileHandlerSettings extends FileHandler {
	public FileHandlerSettings() {
		super(new File("settings.txt"));
	}
	
	@Override
	public void saveFile() {
		ArrayList<String> contents = new ArrayList<String>();
		for (Setting setting : Falcon2.getSettingsManager().getSettings()) {
			String s = ":";
			if (setting.getSettingType() == Setting.SettingType.BOOLEAN)
				s += ((Boolean) setting.getSetting() ? "True" : "False");
			else if (setting.getSettingType() == Setting.SettingType.DOUBLE)
				s += (Double) setting.getSetting();
			else if (setting.getSettingType() == Setting.SettingType.FLOAT)
				s += (Float) setting.getSetting();
			else if (setting.getSettingType() == Setting.SettingType.INTEGER)
				s += (Integer) setting.getSetting();
			contents.add(setting.getSettingName() + s + "\r\n");
		}
		FileUtils.writeFile(getFile(), contents);
	}
	
	@Override
	public void loadFile() {
		if (!doesFileExist())
			return;
		ArrayList<String> contents = FileUtils.readFile(getFile());
		for (String line : contents) {
			line = line.trim();
			if (line.length() > 0) {
				String split[] = line.split(":");
				for (Setting setting : Falcon2.getSettingsManager().getSettings()) {
					if (setting.getSettingName().equals(split[0])) {
						if (setting.getSettingType() == Setting.SettingType.BOOLEAN)
							setting.setSetting(Boolean.parseBoolean(split[1]));
						else if (setting.getSettingType() == Setting.SettingType.DOUBLE)
							setting.setSetting(Double.parseDouble(split[1]));
						else if (setting.getSettingType() == Setting.SettingType.FLOAT)
							setting.setSetting(Float.parseFloat(split[1]));
						else if (setting.getSettingType() == Setting.SettingType.INTEGER)
							setting.setSetting(Integer.parseInt(split[1]));
					}
				}
			}
		}
	}
}
